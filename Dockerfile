# Build stage
FROM maven:3.8.1-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

# Package stage
FROM openjdk:11.0.4-jre-slim
COPY --from=build /home/app/target/*.jar app.jar
ENV MYSQL_HOST=103.163.118.80
ENV MYSQL_USERNAME=root
ENV MYSQL_PASSWORD=root
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]