package com.ptit.hrmanagement.service;

import com.ptit.hrmanagement.model.*;
import com.ptit.hrmanagement.model.Project;
import com.ptit.hrmanagement.repository.DepartmentRepository;
import com.ptit.hrmanagement.repository.EmployeeRepository;
import com.ptit.hrmanagement.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProjectService {
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    EmployeeRepository employeeRepository;

    public Page<Project> getAll(Pageable pageable) {
        return projectRepository.findAll(pageable);
    }

    public Project update(Project project) {
        if( null != project.getId()){
            Project projectSearch = projectRepository.findById(project.getId()).orElse(null);
            if(projectSearch!=null){
                String oldVal = projectSearch.getName();
                String newVal = project.getName();

                // modify department
                List<Department> departments = departmentRepository.findAllByProjectContaining(projectSearch.getName());
                for(Department d : departments){
                    String projectName = d.getProject();
                    d.setProject(projectName.replace(oldVal,newVal));
                }
                departmentRepository.saveAll(departments);


                // modify employee
                List<Employee> employees = employeeRepository.findAllByProjectContaining(oldVal);
                for(Employee e : employees){
                    String projectName = e.getProject();
                    e.setProject(projectName.replace(oldVal,newVal));
                }
                departmentRepository.saveAll(departments);
            }
        }
        return projectRepository.save(project);
    }

    public Project getOne(Integer id) {
        return projectRepository.findById(id).orElse(null);
    }

    public void delete(Integer id) {
        projectRepository.deleteById(id);
    }
}
