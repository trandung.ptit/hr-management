package com.ptit.hrmanagement.service;

import com.ptit.hrmanagement.model.CustomerCategory;
import com.ptit.hrmanagement.model.CustomerCategory;
import com.ptit.hrmanagement.repository.CustomerCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerCategoryService {
    @Autowired
    CustomerCategoryRepository customerCategoryRepository;

    public Page<CustomerCategory> getAll(Pageable pageable) {
        return customerCategoryRepository.findAll(pageable);
    }

    public CustomerCategory update(CustomerCategory customerCategory) {
        return customerCategoryRepository.save(customerCategory);
    }

    public CustomerCategory getOne(Integer id) {
        return customerCategoryRepository.findById(id).orElse(null);
    }

    public void delete(Integer id) {
        customerCategoryRepository.deleteById(id);
    }

    public Page<CustomerCategory> getAllStatusTrue(Pageable pageable) {
        return customerCategoryRepository.findAllByStatusTrue(pageable);
    }
}
