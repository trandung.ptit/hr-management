package com.ptit.hrmanagement.service;

import com.ptit.hrmanagement.model.Project;
import com.ptit.hrmanagement.model.ProjectType;
import com.ptit.hrmanagement.model.ProjectType;
import com.ptit.hrmanagement.repository.ProjectRepository;
import com.ptit.hrmanagement.repository.ProjectTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProjectTypeService {
    @Autowired
    ProjectTypeRepository projectTypeRepository;

    @Autowired
    ProjectRepository projectRepository;

    public Page<ProjectType> getAll(Pageable pageable) {
        return projectTypeRepository.findAll(pageable);
    }

    public ProjectType update(ProjectType projectType) {
        if( null != projectType.getId()){
            ProjectType projectTypeSearch = projectTypeRepository.findById(projectType.getId()).orElse(null);
            if(projectTypeSearch!=null){
                projectRepository.updateProjectType(projectTypeSearch.getName(),projectType.getName());
            }
        }
        return projectTypeRepository.save(projectType);
    }

    public ProjectType getOne(Integer id) {
        return projectTypeRepository.findById(id).orElse(null);
    }

    public void delete(Integer id) {
        projectTypeRepository.deleteById(id);
    }

    public Page<ProjectType> getAllStatusTrue(Pageable pageable) {
        return projectTypeRepository.findAllByStatusTrue(pageable);
    }
}
