package com.ptit.hrmanagement.service;

import com.ptit.hrmanagement.model.*;
import com.ptit.hrmanagement.model.TechStack;
import com.ptit.hrmanagement.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TechStackService {
    @Autowired
    TechStackRepository techStackRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    DepartmentRepository departmentRepository;

    public Page<TechStack> getAll(Pageable pageable) {
        return techStackRepository.findAll(pageable);
    }

    public TechStack update(TechStack techStack) {
        if( null != techStack.getId()){
            TechStack techStackSearch = techStackRepository.findById(techStack.getId()).orElse(null);
            if(techStackSearch!=null){
                String oldVal = techStackSearch.getName();
                String newVal = techStack.getName();


                //modifier project
                List<Project> projects = projectRepository.findAllByTechStackContaining(oldVal);
                for (Project p : projects){
                    String techStackName = p.getTechStack();
                    p.setTechStack(techStackName.replace(oldVal,newVal));
                }
                projectRepository.saveAll(projects);

                //modifier employee
                List<Employee> employees = employeeRepository.findAllByTechStackContaining(oldVal);
                for (Employee e : employees){
                    String techStackName = e.getTechStack();
                    e.setTechStack(techStackName.replace(oldVal,newVal));
                }
                employeeRepository.saveAll(employees);


                //modifier department
                List<Department> departments = departmentRepository.findAllByTechStackContaining(oldVal);
                for (Department d : departments){
                    String techStackName = d.getTechStack();
                    d.setTechStack(techStackName.replace(oldVal,newVal));
                }
                departmentRepository.saveAll(departments);
            }
        }
        return techStackRepository.save(techStack);
    }

    public TechStack getOne(Integer id) {
        return techStackRepository.findById(id).orElse(null);
    }

    public void delete(Integer id) {
        techStackRepository.deleteById(id);
    }

    public Page<TechStack> getAllStatusTrue(Pageable pageable) {
        return techStackRepository.findAllByStatusTrue(pageable);
    }
}
