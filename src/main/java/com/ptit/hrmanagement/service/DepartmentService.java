package com.ptit.hrmanagement.service;

import com.ptit.hrmanagement.model.Department;
import com.ptit.hrmanagement.model.Department;
import com.ptit.hrmanagement.model.TechStack;
import com.ptit.hrmanagement.repository.DepartmentRepository;
import com.ptit.hrmanagement.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    ProjectRepository projectRepository;

    public Page<Department> getAll(Pageable pageable) {
        return departmentRepository.findAll(pageable);
    }

    public Department update(Department department) {
        if( null != department.getId()){
            Department departmentSearch = departmentRepository.findById(department.getId()).orElse(null);
            if(departmentSearch !=null){
                String oldVal = departmentSearch.getName();
                String newVal = department.getName();
                projectRepository.updateDepartment(oldVal,newVal);
            }

        }
        return departmentRepository.save(department);
    }

    public Department getOne(Integer id) {
        return departmentRepository.findById(id).orElse(null);
    }

    public void delete(Integer id) {
        departmentRepository.deleteById(id);
    }
}
