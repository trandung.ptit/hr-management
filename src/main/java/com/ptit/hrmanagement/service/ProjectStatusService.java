package com.ptit.hrmanagement.service;

import com.ptit.hrmanagement.model.ProjectStatus;
import com.ptit.hrmanagement.model.ProjectType;
import com.ptit.hrmanagement.repository.ProjectRepository;
import com.ptit.hrmanagement.repository.ProjectStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProjectStatusService {

    @Autowired
    ProjectStatusRepository projectStatusRepository;

    @Autowired
    ProjectRepository projectRepository;

    public Page<ProjectStatus> getAll(Pageable pageable) {
        return projectStatusRepository.findAll(pageable);
    }

    public ProjectStatus update(ProjectStatus projectStatus) {
        if( null != projectStatus.getId()){
            ProjectStatus projectStatusSearch = projectStatusRepository.findById(projectStatus.getId()).orElse(null);
            if(projectStatusSearch!=null){
                projectRepository.updateProjectStatus(projectStatusSearch.getName(),projectStatus.getName());
            }
        }
        return projectStatusRepository.save(projectStatus);
    }

    public ProjectStatus getOne(Integer id) {
        return projectStatusRepository.findById(id).orElse(null);
    }

    public void delete(Integer id) {
        projectStatusRepository.deleteById(id);
    }

    public Page<ProjectStatus> getAllStatusTrue(Pageable pageable) {
        return projectStatusRepository.findAllByStatusTrue(pageable);
    }
}
