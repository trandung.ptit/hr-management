package com.ptit.hrmanagement.service;

import com.ptit.hrmanagement.model.*;
import com.ptit.hrmanagement.model.Employee;
import com.ptit.hrmanagement.repository.DepartmentRepository;
import com.ptit.hrmanagement.repository.EmployeeRepository;
import com.ptit.hrmanagement.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    ProjectRepository projectRepository;

    public Page<Employee> getAll(Pageable pageable) {
        return employeeRepository.findAll(pageable);
    }

    public Employee update(Employee employee) {
        if( null != employee.getId()){
            Employee employeeSearch = employeeRepository.findById(employee.getId()).orElse(null);
            if (employeeSearch != null){
                String oldVal = employeeSearch.getName();
                String newVal = employee.getName();

                // modify department
                List<Department> departments = departmentRepository.findAllByEmployeeContaining(oldVal);
                for(Department d : departments){
                    String employeeName = d.getEmployee();
                    d.setEmployee(employeeName.replace(oldVal,newVal));
                }
                departmentRepository.saveAll(departments);

                // modify project
                List<Project> projects = projectRepository.findAllByEmployeeContaining(oldVal);
                for(Project p : projects){
                    String employeeName = p.getEmployee();
                    p.setEmployee(employeeName.replace(oldVal,newVal));
                }
                projectRepository.saveAll(projects);
            }
        }
        return employeeRepository.save(employee);
    }

    public Employee getOne(Integer id) {
        return employeeRepository.findById(id).orElse(null);
    }

    public void delete(Integer id) {
        employeeRepository.deleteById(id);
    }
}
