package com.ptit.hrmanagement.controller.api;

import com.ptit.hrmanagement.controller.response.ResponseBodyDTO;
import com.ptit.hrmanagement.model.Project;
import com.ptit.hrmanagement.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/projects")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @GetMapping
    public ResponseEntity<ResponseBodyDTO<Project>> getAll(Pageable pageable) {
        Page<Project> result = projectService.getAll(pageable);
        ResponseBodyDTO<Project> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ResponseBodyDTO<Project>> update(@RequestBody Project request) {
        Project result = projectService.update(request);
        ResponseBodyDTO<Project> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<ResponseBodyDTO<Project>> getOne(@PathVariable Integer id) {
        Project result = projectService.getOne(id);
        ResponseBodyDTO<Project> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<ResponseBodyDTO<Project>> delete(@PathVariable Integer id) {
        projectService.delete(id);
        ResponseBodyDTO<Project> response = new ResponseBodyDTO<>(null, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}