package com.ptit.hrmanagement.controller.api;

import com.ptit.hrmanagement.controller.response.ResponseBodyDTO;
import com.ptit.hrmanagement.model.CustomerCategory;
import com.ptit.hrmanagement.model.Department;
import com.ptit.hrmanagement.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/departments")
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @GetMapping
    public ResponseEntity<ResponseBodyDTO<Department>> getAll(Pageable pageable) {
        Page<Department> result = departmentService.getAll(pageable);
        ResponseBodyDTO<Department> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ResponseBodyDTO<Department>> update(@RequestBody Department request) {
        Department result = departmentService.update(request);
        ResponseBodyDTO<Department> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<ResponseBodyDTO<Department>> getOne(@PathVariable Integer id) {
        Department result = departmentService.getOne(id);
        ResponseBodyDTO<Department> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<ResponseBodyDTO<Department>> delete(@PathVariable Integer id) {
        departmentService.delete(id);
        ResponseBodyDTO<Department> response = new ResponseBodyDTO<>(null, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}