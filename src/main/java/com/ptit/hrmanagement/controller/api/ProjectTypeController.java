package com.ptit.hrmanagement.controller.api;

import com.ptit.hrmanagement.controller.response.ResponseBodyDTO;
import com.ptit.hrmanagement.model.Project;
import com.ptit.hrmanagement.model.ProjectStatus;
import com.ptit.hrmanagement.model.ProjectType;
import com.ptit.hrmanagement.service.ProjectTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/project-types")
public class ProjectTypeController {

    @Autowired
    ProjectTypeService projectTypeService;

    @GetMapping
    public ResponseEntity<ResponseBodyDTO<ProjectType>> getAll(Pageable pageable) {
        Page<ProjectType> result = projectTypeService.getAll(pageable);
        ResponseBodyDTO<ProjectType> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ResponseBodyDTO<ProjectType>> update(@RequestBody ProjectType request) {
        ProjectType result = projectTypeService.update(request);
        ResponseBodyDTO<ProjectType> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getAllActive")
    public ResponseEntity<ResponseBodyDTO<ProjectType>> getAllStatusTrue(Pageable pageable) {
        Page<ProjectType> result = projectTypeService.getAllStatusTrue(pageable);
        ResponseBodyDTO<ProjectType> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<ResponseBodyDTO<ProjectType>> getOne(@PathVariable Integer id) {
        ProjectType result = projectTypeService.getOne(id);
        ResponseBodyDTO<ProjectType> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<ResponseBodyDTO<ProjectType>> delete(@PathVariable Integer id) {
        projectTypeService.delete(id);
        ResponseBodyDTO<ProjectType> response = new ResponseBodyDTO<>(null, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}