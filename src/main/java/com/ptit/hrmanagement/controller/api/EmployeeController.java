package com.ptit.hrmanagement.controller.api;

import com.ptit.hrmanagement.controller.response.ResponseBodyDTO;
import com.ptit.hrmanagement.model.Department;
import com.ptit.hrmanagement.model.Employee;
import com.ptit.hrmanagement.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<ResponseBodyDTO<Employee>> getAll(Pageable pageable) {
        Page<Employee> result = employeeService.getAll(pageable);
        ResponseBodyDTO<Employee> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ResponseBodyDTO<Employee>> update(@RequestBody Employee request) {
        Employee result = employeeService.update(request);
        ResponseBodyDTO<Employee> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<ResponseBodyDTO<Employee>> getOne(@PathVariable Integer id) {
        Employee result = employeeService.getOne(id);
        ResponseBodyDTO<Employee> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<ResponseBodyDTO<Employee>> delete(@PathVariable Integer id) {
        employeeService.delete(id);
        ResponseBodyDTO<Employee> response = new ResponseBodyDTO<>(null, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}