package com.ptit.hrmanagement.controller.api;

import com.ptit.hrmanagement.controller.response.ResponseBodyDTO;
import com.ptit.hrmanagement.model.CustomerCategory;
import com.ptit.hrmanagement.service.CustomerCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/customer-categories")
public class CustomerCategoryController {

    @Autowired
    CustomerCategoryService customerCategoryService;

    @GetMapping
    public ResponseEntity<ResponseBodyDTO<CustomerCategory>> getAll(Pageable pageable) {
        Page<CustomerCategory> result = customerCategoryService.getAll(pageable);
        ResponseBodyDTO<CustomerCategory> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ResponseBodyDTO<CustomerCategory>> update(@RequestBody CustomerCategory request) {
        CustomerCategory result = customerCategoryService.update(request);
        ResponseBodyDTO<CustomerCategory> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<ResponseBodyDTO<CustomerCategory>> getOne(@PathVariable Integer id) {
        CustomerCategory result = customerCategoryService.getOne(id);
        ResponseBodyDTO<CustomerCategory> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getAllActive")
    public ResponseEntity<ResponseBodyDTO<CustomerCategory>> getAllStatusTrue(Pageable pageable) {
        Page<CustomerCategory> result = customerCategoryService.getAllStatusTrue(pageable);
        ResponseBodyDTO<CustomerCategory> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<ResponseBodyDTO<CustomerCategory>> delete(@PathVariable Integer id) {
        customerCategoryService.delete(id);
        ResponseBodyDTO<CustomerCategory> response = new ResponseBodyDTO<>(null, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}