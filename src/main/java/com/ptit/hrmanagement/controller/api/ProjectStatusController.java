package com.ptit.hrmanagement.controller.api;

import com.ptit.hrmanagement.controller.response.ResponseBodyDTO;
import com.ptit.hrmanagement.model.CustomerCategory;
import com.ptit.hrmanagement.model.ProjectStatus;
import com.ptit.hrmanagement.service.ProjectStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/project-status")
public class ProjectStatusController {

    @Autowired
    ProjectStatusService projectStatusService;

    @GetMapping
    public ResponseEntity<ResponseBodyDTO<ProjectStatus>> getAll(Pageable pageable) {
        Page<ProjectStatus> result = projectStatusService.getAll(pageable);
        ResponseBodyDTO<ProjectStatus> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ResponseBodyDTO<ProjectStatus>> update(@RequestBody ProjectStatus request) {
        ProjectStatus result = projectStatusService.update(request);
        ResponseBodyDTO<ProjectStatus> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getAllActive")
    public ResponseEntity<ResponseBodyDTO<ProjectStatus>> getAllStatusTrue(Pageable pageable) {
        Page<ProjectStatus> result = projectStatusService.getAllStatusTrue(pageable);
        ResponseBodyDTO<ProjectStatus> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<ResponseBodyDTO<ProjectStatus>> getOne(@PathVariable Integer id) {
        ProjectStatus result = projectStatusService.getOne(id);
        ResponseBodyDTO<ProjectStatus> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<ResponseBodyDTO<ProjectStatus>> delete(@PathVariable Integer id) {
        projectStatusService.delete(id);
        ResponseBodyDTO<ProjectStatus> response = new ResponseBodyDTO<>(null, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
