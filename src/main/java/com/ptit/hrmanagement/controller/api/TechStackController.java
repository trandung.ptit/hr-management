package com.ptit.hrmanagement.controller.api;

import com.ptit.hrmanagement.controller.response.ResponseBodyDTO;
import com.ptit.hrmanagement.model.ProjectStatus;
import com.ptit.hrmanagement.model.ProjectType;
import com.ptit.hrmanagement.model.TechStack;
import com.ptit.hrmanagement.service.TechStackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/tech-stacks")
public class TechStackController {

    @Autowired
    TechStackService techStackService;

    @GetMapping
    public ResponseEntity<ResponseBodyDTO<TechStack>> getAll(Pageable pageable) {
        Page<TechStack> result = techStackService.getAll(pageable);
        ResponseBodyDTO<TechStack> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ResponseBodyDTO<TechStack>> update(@RequestBody TechStack request) {
        TechStack result = techStackService.update(request);
        ResponseBodyDTO<TechStack> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getAllActive")
    public ResponseEntity<ResponseBodyDTO<TechStack>> getAllStatusTrue(Pageable pageable) {
        Page<TechStack> result = techStackService.getAllStatusTrue(pageable);
        ResponseBodyDTO<TechStack> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<ResponseBodyDTO<TechStack>> getOne(@PathVariable Integer id) {
        TechStack result = techStackService.getOne(id);
        ResponseBodyDTO<TechStack> response = new ResponseBodyDTO<>(result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<ResponseBodyDTO<TechStack>> delete(@PathVariable Integer id) {
        techStackService.delete(id);
        ResponseBodyDTO<TechStack> response = new ResponseBodyDTO<>(null, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
