package com.ptit.hrmanagement.repository;

import com.ptit.hrmanagement.model.Department;
import com.ptit.hrmanagement.model.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
    Page<Project> findAll(Pageable pageable);

    @Modifying
    @Query("update Project p set p.projectType = :newString where p.projectType = :oldString")
    void updateProjectType(@Param("oldString") String oldString, @Param("newString") String newString);

    @Modifying
    @Query("update Project p set p.projectStatus = :newString where p.projectStatus = :oldString")
    void updateProjectStatus(@Param("oldString") String oldString,@Param("newString") String newString);

    @Modifying
    @Query("update Project p set p.techStack = :newString where p.techStack = :oldString")
    void updateTechStack(@Param("oldString") String oldString,@Param("newString") String newString);

    @Modifying
    @Query("update Project p set p.department = :newString where p.department = :oldString")
    void updateDepartment(@Param("oldString") String oldString,@Param("newString") String newString);

    List<Project> findAllByEmployeeContaining(String employee);
    List<Project> findAllByTechStackContaining(String techStack);
}
