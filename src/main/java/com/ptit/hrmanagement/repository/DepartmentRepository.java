package com.ptit.hrmanagement.repository;

import com.ptit.hrmanagement.model.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
    Page<Department> findAll(Pageable pageable);

    @Modifying
    @Query("update Department d set d.techStack = :newString where d.techStack = :oldString")
    void updateTechStack(@Param("oldString") String oldString,@Param("newString") String newString);

    List<Department> findAllByProjectContaining(String project);
    List<Department> findAllByEmployeeContaining(String employee);

    List<Department> findAllByTechStackContaining(String techStack);
}
