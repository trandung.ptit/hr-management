package com.ptit.hrmanagement.repository;

import com.ptit.hrmanagement.model.CustomerCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerCategoryRepository extends JpaRepository<CustomerCategory, Integer> {
    Page<CustomerCategory> findAll(Pageable pageable);
    Page<CustomerCategory> findAllByStatusTrue(Pageable pageable);
}
