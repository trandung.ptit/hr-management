package com.ptit.hrmanagement.repository;

import com.ptit.hrmanagement.model.ProjectStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectStatusRepository extends JpaRepository<ProjectStatus, Integer> {
    Page<ProjectStatus> findAll(Pageable pageable);
    Page<ProjectStatus> findAllByStatusTrue(Pageable pageable);
}
