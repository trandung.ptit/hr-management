package com.ptit.hrmanagement.repository;

import com.ptit.hrmanagement.model.ProjectType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectTypeRepository extends JpaRepository<ProjectType, Integer> {
    Page<ProjectType> findAll(Pageable pageable);
    Page<ProjectType> findAllByStatusTrue(Pageable pageable);
}
