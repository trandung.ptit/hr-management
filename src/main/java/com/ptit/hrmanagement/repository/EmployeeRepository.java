package com.ptit.hrmanagement.repository;

import com.ptit.hrmanagement.model.Department;
import com.ptit.hrmanagement.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    Page<Employee> findAll(Pageable pageable);

    @Modifying
    @Query("update Employee e set e.techStack = :newString where e.techStack = :oldString")
    void updateTechStack(@Param("oldString") String oldString, @Param("newString") String newString);

    List<Employee> findAllByProjectContaining(String project);

    List<Employee> findAllByTechStackContaining(String techStack);
}
