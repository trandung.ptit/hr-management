package com.ptit.hrmanagement.repository;

import com.ptit.hrmanagement.model.TechStack;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TechStackRepository extends JpaRepository<TechStack, Integer> {
    Page<TechStack> findAll(Pageable pageable);
    Page<TechStack> findAllByStatusTrue(Pageable pageable);
}
