package com.ptit.hrmanagement.repository;

import com.ptit.hrmanagement.model.EmployeeTechStack;
import com.ptit.hrmanagement.model.id.EmployeeTechStackId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeTechStackRepository extends JpaRepository<EmployeeTechStack, EmployeeTechStackId> {
}
