package com.ptit.hrmanagement.model;

import com.ptit.hrmanagement.model.id.EmployeeTechStackId;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Table(name = "employee_tech_stack")
@Entity
@IdClass(EmployeeTechStackId.class)
public class EmployeeTechStack {
    @Id
    @Column(name = "tech_stack_id")
    private Integer techStackId;

    @Id
    @Column(name = "employee_id")
    private Integer employeeId;

    @Column
    private Float experience;

    @Column(name = "framework_id")
    private Integer frameworkId;

    @Column
    private Boolean status;
}
