package com.ptit.hrmanagement.model.id;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ProjectEmployeeId implements Serializable {
    private String projectId;
    private String employeeId;
}
