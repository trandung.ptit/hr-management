package com.ptit.hrmanagement.model.id;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
public class EmployeeTechStackId implements Serializable {
    private String techStackId;
    private String employeeId;
}
