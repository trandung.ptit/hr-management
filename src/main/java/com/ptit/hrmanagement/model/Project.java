package com.ptit.hrmanagement.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Table(name = "project")
@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; // tự động gen

    @Column
    private String name;

    @Column
    private String description;

    @Column(name = "project_type")
    private String projectType;

    @Column(name = "project_status")
    private String projectStatus;

    @Column(name = "tech_stack")
    private String techStack;

    @Column(name = "department")
    private String department;

    @Column(name = "employee")
    private String employee;
}
