package com.ptit.hrmanagement.model;

import com.ptit.hrmanagement.model.id.EmployeeTechStackId;
import com.ptit.hrmanagement.model.id.ProjectEmployeeId;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Table(name = "project_employee")
@Entity
@IdClass(ProjectEmployeeId.class)
public class ProjectEmployee {
    @Id
    @Column(name = "project_id")
    private Integer projectId;

    @Id
    @Column(name = "employee_id")
    private Integer employeeId;

    @Column
    private Boolean status;
}
